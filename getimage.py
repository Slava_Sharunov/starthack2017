import requests
from bs4 import BeautifulSoup

# define the url to be scraped
url = 'https://www.google.ch/search?q=anime&espv=2&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjlxO6SjeHSAhVBsCwKHSYZCMQQ_AUIBigB&biw=1996&bih=1061'

# top line is using the attached "tower.html" as source, bottom line is using the url. The html file contains the source of the above url.
#page = open('tower.html', 'r').read()
page = requests.get(url).text

soup = BeautifulSoup(page, 'html.parser')

for raw_img in soup.find_all('img'):
  link = raw_img.get('src')
  if link:
    print(link)
